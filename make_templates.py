import configparser
import shutil

import geopandas as gpd
import pandas as pd
import psycopg2 as pg
from shapely import wkt
import os
import dbf
from sqlalchemy import create_engine
import openpyxl  # lib need to be installed but not imported, used by pandas to export xlsx


__author__ = 'Simon Makwarth <simak@mst.dk>'
__description__ = '''
    This script creates gis templates for alle themes in "template_names" list defined within the script.
    The gis templates are made from the associated table within the grukos database (e.g. iol => gvkort.iol_alle)
    
    Since the geopandas library cannot write an empty shapefile, the script is creating a template using a inserted row.
    The row is then subsequently removed using the dbf library, 
    thus creating an empty shapefile with the correct column-names and datatypes.
'''
# TODO: Find a way to handle items in list not_handled_in_script

not_handled_in_script = [
    'potentiale_synkronpejlerunde',
    '3d_reduceret_lertykkelse',
    'saarbarhed_grundvandsdan',
    'grddanopl_partikelbaner_lin',
    'grddanopl_partikelbaner_pkt'
]


class Database:

    def __init__(self):
        self.ini_file = 'F:/GKO/data/grukos/db_credentials/reader/reader.ini'

    def parse_db_credentials(self, database_name='GRUKOSREADER'):
        config = configparser.ConfigParser()
        config.read(self.ini_file)

        usr_read = config['GRUKOSREADER']['userid']
        pw_read = config[f'{database_name}']['password']
        host = config[f'{database_name}']['host']
        port = config[f'{database_name}']['port']
        dbname = config[f'{database_name}']['dbname']

        return usr_read, pw_read, host, port, dbname

    def connect_to_pg_db(self):
        usr_read, pw_read, host, port, database = self.parse_db_credentials()
        # Construct connection string
        connection_string = f'postgresql://{usr_read}:{pw_read}@{host}:{port}/{database}'

        # Create SQLAlchemy engine
        engine = create_engine(connection_string)
        pg_con = engine.connect()

        return pg_con

    def connect_to_pg_db_old(self):
        usr_read, pw_read, host, port, database = self.parse_db_credentials()
        try:
            pg_con = pg.connect(
                host=host,
                port=port,
                database=database,
                user=usr_read,
                password=pw_read
            )
        except Exception as e:
            print(e)
            raise ConnectionError('Could not connect to database')

        return pg_con

    def fetch_db_template(self, sql):
        # connect to grukos db

        with self.connect_to_pg_db() as con:
            df_columnname_datatype = pd.read_sql(sql, con=con)
            columns_names = df_columnname_datatype['column_name']
            data_types = df_columnname_datatype['data_type']

        return columns_names, data_types


class CreateTemplate:
    def __init__(self, dst_path, src_path='./Skabelon_nummer_GKO navn'):
        self.src_path = src_path
        self.dst_path = dst_path

    @staticmethod
    def fetch_template(table_name):
        sql = f'''
            SELECT
                pg_attribute.attname AS column_name,
                pg_catalog.format_type(pg_attribute.atttypid, pg_attribute.atttypmod) AS data_type
            FROM
                pg_catalog.pg_attribute
            INNER JOIN
                pg_catalog.pg_class ON pg_class.oid = pg_attribute.attrelid
            INNER JOIN
                pg_catalog.pg_namespace ON pg_namespace.oid = pg_class.relnamespace
            WHERE
                pg_attribute.attnum > 0
                AND NOT pg_attribute.attisdropped
                AND pg_namespace.nspname = 'gvkort'
                AND pg_class.relname = '{table_name}'
            ORDER BY
                attnum ASC;
        '''
        d = Database()
        columns_names, data_types = d.fetch_db_template(sql)
        return columns_names, data_types

    def fetch_template_info(self):
        # make shapefile for each template
        template_info = [
            {
                'shp_name': 'afgpolygon',
                'path': f'{self.src_path}/00 Projektoplysninger afgrænsningspolygon og modelafgrænsninger/Afgrænsningspolygon',
                'columns': ['kortnavn', 'kommentar', 'geom']
            },
            {
                'shp_name': 'modelomrgeokemisk',
                'path': f'{self.src_path}/00 Projektoplysninger afgrænsningspolygon og modelafgrænsninger/Modelafgrænsninger',
                'columns': ['kortnavn', 'konsulent', 'dynamik', 'kommentar', 'geom']
            },
            {
                'shp_name': 'modelomrgeologisk',
                'path': f'{self.src_path}/00 Projektoplysninger afgrænsningspolygon og modelafgrænsninger/Modelafgrænsninger',
                'columns': [
                    'kortnavn', 'modelaf', 'modelapp', 'programver', 'modeltype', 'konsulent', 'kommentar', 'geom'
                ]
            },
            {
                'shp_name': 'modelomrhydrologisk',
                'path': f'{self.src_path}/00 Projektoplysninger afgrænsningspolygon og modelafgrænsninger/Modelafgrænsninger',
                'columns': [
                    'kortnavn', 'modelid', 'modelaf', 'modelapp', 'programver', 'modeltype', 'konsulent',
                    'grdvanddan', 'dynamik', 'kommentar', 'geom'
                ]
            },
            {
                'shp_name': 'modelomrhydrostratigrafisk',
                'path': f'{self.src_path}/00 Projektoplysninger afgrænsningspolygon og modelafgrænsninger/Modelafgrænsninger',
                'columns': [
                    'kortnavn', 'modelaf', 'modelapp', 'programver', 'modeltype', 'konsulent', 'kommentar', 'geom'
                ]
            },
            {
                'shp_name': 'di',
                'path': f'{self.src_path}/16 Drikkevandsinteresser',
                'columns': ['kortnavn', 'list_di_id', 'kommentar', 'geom']
            },
            {
                'shp_name': 'fi',
                'path': f'{self.src_path}/14 Nitratfølsomme indvindingsområder',
                'columns': ['kortnavn', 'list_io_id', 'kommentar', 'geom']
            },
            {
                'shp_name': 'io',
                'path': f'{self.src_path}/15 Indsatsområder',
                'columns': ['kortnavn', 'list_io_id', 'kommentar', 'geom']
            },
            {
                'shp_name': 'iol_alle',
                'path': f'{self.src_path}/12 Indvindingsoplande afgrænsning/indvindingsoplande',
                'columns': [
                    'kortnavn', 'anl_id', 'anl_navn', 'idenf_osd', 'anlaeg_url', 'boringer', 'modeltype', 'modelkode',
                    'dynamisk', 'indvtil', 'tilprbor', 'magasin', 'rapporturl', 'kommentar', 'geom'
                ]
            },
            {
                'shp_name': 'iol_boringer',
                'path': f'{self.src_path}/12 Indvindingsoplande afgrænsning/boringer_i_indvindingsopland',
                'columns': ['kortnavn', 'anl_id', 'anl_navn', 'dgunr', 'indtagsnr', 'indvprocnt', 'kommentar', 'geom']
            },
            {
                'shp_name': 'iol_par',
                'path': f'{self.src_path}/12 Indvindingsoplande afgrænsning/indvindingsoplande_partikler',
                'columns': [
                    'kortnavn', 'anl_id', 'anl_navn', 'transptid', 'trans_enhe', 'idenf_osd', 'modeltype', 'modelkode',
                    'dynamisk', 'kommentar', 'geom'
                ]
            },
            {
                'shp_name': 'iol_partikelbaner_lin',
                'path': f'{self.src_path}/12 Indvindingsoplande afgrænsning/indvindingsoplande_partikler',
                'columns': [
                    'kortnavn', 'anl_id', 'anl_navn', 'idenf_osd', 'modeltype', 'modelkode', 'dynamisk', 'geom'
                ]
            },
            {
                'shp_name': 'iol_partikelbaner_pkt',
                'path': f'{self.src_path}/12 Indvindingsoplande afgrænsning/indvindingsoplande_partikler',
                'columns': [
                    'kortnavn', 'anl_id', 'anl_navn', 'transptid', 'trans_enhe', 'z_kote', 'enhed', 'koteenhed',
                    'part_id', 'idenf_osd', 'modeltype', 'modelkode', 'dynamisk', 'kommentar', 'geom'
                ]
            },
            {
                'shp_name': 'iol_stokastik_pkt',
                'path': f'{self.src_path}/12 Indvindingsoplande afgrænsning/stokastiske_indvindingsoplande',
                'columns': [
                    'kortnavn', 'anl_id', 'anl_navn', 'modeltype', 'modelkode', 'dynamisk', 'indvindtil', 'idenf_osd',
                    'procent', 'kommentar', 'geom'
                ]
            },
            {
                'shp_name': 'bnbo',
                'path': f'{self.src_path}/17 BNBO',
                'columns': [
                    'kortnavn', 'modelid', 'anl_id', 'anl_navn', 'dgunr', 'indtagsnr', 'tilladelse', 'stroemning', 'k',
                    'porositet', 'metode', 'kommunenav', 'regionnavn', 'rapporturl', 'kommentar', 'geom'
                ]
            },
            {
                'shp_name': 'bnbo_boringer',
                'path': f'{self.src_path}/17 BNBO',
                'columns': [
                    'kortnavn', 'modelid', 'anl_id', 'anl_navn', 'dgunr', 'indtag', 'magasin', 'indvprocnt',
                    'rapporturl', 'kommentar', 'geom'
                ]
            },
            {
                'shp_name': 'bnbo_partikler',
                'path': f'{self.src_path}/17 BNBO',
                'columns': [
                    'kortnavn', 'modelid', 'anl_id', 'anl_navn', 'dgunr', 'indtag', 'magasin', 'transptid',
                    'trans_unit', 'kommentar', 'geom'
                ]
            },
            {
                'shp_name': 'grddanopl_par',
                'path': f'{self.src_path}/13 Indvindingsoplande, grundvandsdannende del',
                'columns': [
                    'kortnavn', 'anl_id', 'anl_navn', 'transptid', 'trans_enh', 'idenf_osd', 'modeltype',
                    'modelkode', 'dynamisk', 'kommentar', 'geom'
                ]
            },
            {
                'shp_name': 'grdvdanomr',
                'path': f'{self.src_path}/07 Grundvandsdannelse fra terræn til magasiner',
                'columns': ['kortnavn', 'magasin', 'flux', 'fluxenhed', 'kommentar', 'geom']
            },
            {
                'shp_name': 'magasinlag_pktgrid',
                'path': f'{self.src_path}/04 Magasinudbredelse og top-bund',
                'columns': [
                    'kortnavn', 'bundkote', 'topkote', 'tykkelse', 'magasinlag', 'enhed', 'koteenhed', 'kommentar',
                    'geom'
                ]
            },
            {
                'shp_name': 'pejlinger',
                'path': f'{self.src_path}/01 Boringer- Udførsel, jordprøveanalyser, '
                        f'lokalisering, pejlinger, prøvepumpninger/Lokaliseringsdata og Pejlinger',
                'columns': [
                    'kortnavn', 'dgunr', 'indtagnr', 'kote', 'pejledato', 'magasin', 'pejletype', 'kommentar', 'geom'
                ]
            },
            {
                'shp_name': 'saarbarhed_nitrat',
                'path': f'{self.src_path}/11 Sårbarhedsvurdering',
                'columns': ['kortnavn', 'magasin', 'list_saarb', 'kommentar', 'geom']
            },
            {
                'shp_name': 'saarbarhed_redox_pkt',
                'path': f'{self.src_path}/11 Sårbarhedsvurdering/Redoxgrænse',
                'columns': ['kortnavn', 'farveskift', 'metodik', 'enhed', 'koteenhed', 'dgunr', 'kommentar', 'geom']
            },
            {
                'shp_name': 'saarbarhed_redox_interp_pkt',
                'path': f'{self.src_path}/11 Sårbarhedsvurdering/Redoxgrænse',
                'columns': ['kortnavn', 'oevre', 'enhed', 'kommentar', 'geom']
            },
            {
                'shp_name': 'vandudveksling',
                'path': f'{self.src_path}/08 Vandudveksling mellem magasiner',
                'columns': ['kortnavn', 'magasin', 'flux', 'fluxenhed', 'kommentar', 'geom']
            },
            {
                'shp_name': 'lertykkelse_pktgrid',
                'path': f'{self.src_path}/05 Dæklagstykkelse magasinspecifikt',
                'columns': ['kortnavn', 'magasin', 'lerlag', 'tykkelse', 'list_lerlagtilhoer_id', 'kommentar', 'geom']
            },
            {
                'shp_name': 'magasinlag_poly',
                'path': f'{self.src_path}/11 Sårbarhedsvurdering/Arealangivelse_af_drikkevandsmagasin_for_sårbarhed',
                'columns': [
                    'kortnavn', 'bundkote', 'topkote', 'tykkelse', 'magasinlag', 'enhed', 'koteenhed', 'kommentar',
                    'geom'
                ]
            },
            {
                'shp_name': 'potentiale_lin',
                'path': f'{self.src_path}/06 Potentialekort',
                'columns': [
                    'kortnavn', 'magasin', 'kote', 'enhed', 'koteenhed', 'list_potentialetype_id', 'oprind',
                    'kommentar', 'geom'
                ]
            },
            {
                'shp_name': 'reduceret_lertykkelse_pktgrid',
                'path': f'{self.src_path}/11 Sårbarhedsvurdering/2D_reduceret_lertykkelse',
                'columns': ['kortnavn', 'tykkelse', 'magasin', 'list_lerlagtilhoer_id', 'kommentar', 'geom']
            },
            {
                'shp_name': 'list_di',
                'path': f'{self.src_path}/fælles_listetabeller',
                'columns': ['list_di_id', 'navn']
            },
            {
                'shp_name': 'list_io',
                'path': f'{self.src_path}/fælles_listetabeller',
                'columns': ['list_io_id', 'navn']
            },
            {
                'shp_name': 'list_lerlagtilhoer',
                'path': f'{self.src_path}/fælles_listetabeller',
                'columns': ['list_lerlagtilhoer_id', 'navn']
            },
            {
                'shp_name': 'list_potentialetype',
                'path': f'{self.src_path}/fælles_listetabeller',
                'columns': ['list_potentialetype_id', 'type']
            },
            {
                'shp_name': 'list_saarbarhed',
                'path': f'{self.src_path}/fælles_listetabeller',
                'columns': ['list_saarbarhed_id', 'saarbarhed']
            }
        ]

        return template_info

    def delete_template_folder(self):
        # delete existing template-folder with subfolders
        print(f'Deleting folder and subfolders: {self.src_path}')
        if os.path.exists(self.src_path):
            os.system(f'rmdir /S /Q "{self.src_path}"')
            # shutil.rmtree(self.src_path)

    @staticmethod
    def create_template_folder(path):
        if not os.path.exists(path):
            os.makedirs(path)

    def create_idle_template_folders(self):
        # make empty folders
        print(f'Creating all empty folder in template structure: {self.src_path}')
        os.makedirs(f'{self.src_path}/01 Boringer- Udførsel, jordprøveanalyser, '
                    f'lokalisering, pejlinger, prøvepumpninger/Aldersdatering')
        os.makedirs(f'{self.src_path}/01 Boringer- Udførsel, jordprøveanalyser, lokalisering, pejlinger, prøvepumpninger/DGUxx.xxx/Billeder af jordprøver')
        os.makedirs(f'{self.src_path}/01 Boringer- Udførsel, jordprøveanalyser, '
                    f'lokalisering, pejlinger, prøvepumpninger/DGUxx.xxx/Logbøger fra borearbejdet')
        os.makedirs(f'{self.src_path}/01 Boringer- Udførsel, jordprøveanalyser, '
                    f'lokalisering, pejlinger, prøvepumpninger/DGUxx.xxx/Videologs')
        os.makedirs(f'{self.src_path}/01 Boringer- Udførsel, jordprøveanalyser, '
                    'lokalisering, pejlinger, prøvepumpninger/Prøvepumpninger')
        os.makedirs(f'{self.src_path}/01 Boringer- Udførsel, jordprøveanalyser, '
                    'lokalisering, pejlinger, prøvepumpninger/Sedimentkemi')
        os.makedirs(f'{self.src_path}/01 Boringer- Udførsel, jordprøveanalyser, '
                    'lokalisering, pejlinger, prøvepumpninger/Udv Jordprøvebeskr')
        os.makedirs(f'{self.src_path}/02 Vandkemi')
        os.makedirs(f'{self.src_path}/03 Geofysik/Logs, LAS filer')
        os.makedirs(f'{self.src_path}/03 Geofysik/MRS')
        os.makedirs(f'{self.src_path}/03 Geofysik/SSV Projekt')
        os.makedirs(f'{self.src_path}/03 Geofysik/Workbench Projekt')
        os.makedirs(f'{self.src_path}/06 Potentialekort/Pejle- og støttepunkter')
        os.makedirs(f'{self.src_path}/09 Geologisk-hydrostratigrafisk model')
        os.makedirs(f'{self.src_path}/10 Hydrologisk model/Hydrauliske tolkninger')
        os.makedirs(f'{self.src_path}/10 Hydrologisk model/Indv oplande_andre end administrative')
        os.makedirs(f'{self.src_path}/10 Hydrologisk model/Model')
        os.makedirs(f'{self.src_path}/10 Hydrologisk model/Vandføringsdata')
        os.makedirs(f'{self.src_path}/18 Rapporter og GIS projekter/'
                    'Rapportnavn_Rapport IDnr/Samlet GIS præsentation (ArcGIS)')
        os.makedirs(f'{self.src_path}/18 Rapporter og GIS projekter/'
                    'Rapportnavn_Rapport IDnr/Samlet GIS præsentation (MapInfo)/MapInfo projekter')
        os.makedirs(f'{self.src_path}/18 Rapporter og GIS projekter/Rapportnavn_Rapport IDnr/'
                    f'Samlet GIS præsentation (QGIS)/MapInfo projekter')
        os.makedirs(f'{self.src_path}/19 Informations- og baggrundsmateriale')

    @staticmethod
    def create_df_datetype(columns_names, data_types, df):
        for columns_name, data_type in zip(columns_names, data_types):
            if data_type in ('integer', 'bigint', 'int', 'smallint', 'interger64'):
                df[columns_name] = pd.Series(-9999, dtype='int')
            elif data_type in ('double precision', 'numeric', 'numeric(20,0)'):
                df[columns_name] = pd.Series(-9999, dtype='float')
            elif data_type == 'boolean':
                df[columns_name] = pd.Series(True, dtype='bool')
            elif data_type in ('character varying(200)', 'character varying(500)', 'character varying(50)',
                               'character varying', 'character varying(250)', 'character varying(100)', 'text',
                               'character varying(254)', 'character varying(222)', 'character varying(256)',
                               'character varying(10)', 'character varying(5)', 'character varying(2)',
                               'character varying(20)', 'character(50)'):
                df[columns_name] = pd.Series('TEMPLATE', dtype='str')
            elif data_type == 'date':
                df[columns_name] = pd.Series('1900-01-01 00:00:00', dtype='str')
            elif data_type == 'geometry(MultiPolygon,25832)':
                txt = 'POLYGON((350000 6420000,900000 6420000,900000 6000000,350000 6000000,350000 6420000))'
                df[columns_name] = pd.Series(txt, dtype='str')
            elif data_type in ('geometry(Point,25832)', 'geometry(MultiPoint,25832)'):
                txt = 'POINT(559882.87 6319704.84)'
                df[columns_name] = pd.Series(txt, dtype='str')
            elif data_type == 'geometry(MultiLineString,25832)':
                txt = 'LINESTRING(350000 6420000,900000 6420000,900000 6000000,350000 6000000)'
                df[columns_name] = pd.Series(txt, dtype='str')
            else:
                df[columns_name] = pd.Series(dtype=data_type)

    @staticmethod
    def df_to_shp(out_path, name, df):
        # transform dataframe into geodataframe with geometry
        # if geometry exists: gdf => export as shp
        # if no geometry exists: gdf => export as spreadsheet
        if 'geom' in df:
            df['geom'] = df['geom'].apply(wkt.loads)
            gdf = gpd.GeoDataFrame(df, geometry='geom', crs='EPSG:25832')

            # export templates into folder at cwd
            shp_path = f'{out_path}/{name}.shp'
            gdf.to_file(shp_path, if_exists='replace', encoding='utf-8')

            # remove the row inserted into template shape
            dbf_file = shp_path[0:-4] + '.dbf'
            table = dbf.Table(dbf_file).open(mode=dbf.READ_WRITE)
            with table as tab:
                for record in dbf.Process(tab):
                    dbf.delete(record)
            table.pack()

        else:
            # df_temp = df.truncate(before=-1, after=-1)
            df_temp = df
            df_temp.to_excel(f'{out_path}/{name}.xlsx', sheet_name='Sheet1', index=False)

    def copy_template_descr(self):
        # copy description pdf to template folder
        pdf_src = f'./mst_vejledning_dataaflevering_raadgiver.pdf'
        pdf_path = f'{self.src_path}/'
        filename = os.path.basename(pdf_src)
        pdf_dst = pdf_path+filename
        self.create_template_folder(pdf_path)
        shutil.copyfile(pdf_src, pdf_dst)

    def copy_project_info(self):
        # copy project-info xlsx to template folder
        xlsx_src = './projektoplysninger.xlsx'
        filename = os.path.basename(xlsx_src)
        xlsx_path = f'{self.src_path}/00 Projektoplysninger, afgrænsningspolygon og modelafgrænsninger/'
        xlsx_dst = xlsx_path+filename
        self.create_template_folder(xlsx_path)
        shutil.copyfile(xlsx_src, xlsx_dst)

    def create_template(self):
        # delete existing folder and creates new empty folders
        self.delete_template_folder()
        self.create_idle_template_folders()

        # copy description and projekt info to main template folder
        self.copy_project_info()
        self.copy_template_descr()

        template_info = self.fetch_template_info()

        for row in template_info:
            df = pd.DataFrame()
            out_path = row['path']
            shp_name = row['shp_name']
            column_names = row['columns']
            print(f'tablename:{shp_name}, shpname:{shp_name}')

            columns_names, data_types = self.fetch_template(shp_name)

            self.create_template_folder(out_path)
            self.create_df_datetype(columns_names=columns_names, data_types=data_types, df=df)

            df = df[column_names]  # sort out unwanted columns
            self.df_to_shp(out_path, shp_name, df)

        # make zip filer from template folder
        shutil.make_archive(f'{self.dst_path}', 'zip', self.src_path)

        # delete folder
        self.delete_template_folder()


# tabeller der genereres en skabelon for, kan ses i tuple kaldet "template_names"
# tabel kolonnerne der frasorteres før shp template genereres ses i tuple kaldet "remove_columns"
output_path = r'C:\Users\b028067\filkassen\grukos_uploader\gis_skabelon_mst'
CreateTemplate(dst_path=output_path).create_template()
